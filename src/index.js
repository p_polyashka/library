const express = require("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const userRoutes = require("./routes/userRoutes");
const libraryRoutes = require("./routes/libraryRoutes");


app.use("/", userRoutes);
app.use("/", libraryRoutes);


app.listen(port, () => {
    console.log(`server listening on port:${port}`)
})
