const {v4: uuidv4} = require("uuid");

class Library {
    constructor() {
        this.shelves = {
            miscellaneous: [],
            fantasy: [],
            mystery: [],
            romance: [],
            novel: []
        };
    }

    createBook(title, author, genre, isbn) {
        const newBook = {
            id: uuidv4(),
            title,
            author,
            genre,
            isbn
        };
        this.shelves.miscellaneous.push(newBook);
        return newBook;
    }


    createShelf(genre) {
        const lowercaseGenre = genre.toLowerCase();

        if (this.shelves[lowercaseGenre]) {
            throw new Error(`Shelf '${genre}' already exists.`);
        }

        this.shelves[lowercaseGenre] = [];
        return this.shelves[lowercaseGenre];
    }

    moveBookToShelf(user, bookId, currentShelf, targetShelf) {
        const index = currentShelf.books.indexOf(bookId);
        if (index !== -1) {
            currentShelf.books.splice(index, 1);
            targetShelf.books.push(bookId);
        }
    }

    getAllBooks() {
        const allBooks = [];
        for (const shelf in this.shelves) {
            allBooks.push(...this.shelves[shelf]);
        }
        return allBooks;
    }

    getAllShelves() {
        return this.shelves;
    }

    searchBooks(criteria) {
        const allBooks = Object.values(this.shelves).flat();

        const results = allBooks.filter(book => {
            return (
                (!criteria.title || book.title.toLowerCase().includes(criteria.title.toLowerCase())) &&
                (!criteria.author || book.author.toLowerCase().includes(criteria.author.toLowerCase())) &&
                (!criteria.isbn || book.isbn.toLowerCase().includes(criteria.isbn.toLowerCase())) &&
                (!criteria.genre || book.genre.toLowerCase().includes(criteria.genre.toLowerCase())) &&
                (!criteria.id || book.id === criteria.id)
            );
        });

        return results;
    }

    getBookById(bookId) {
        for (const shelf of Object.values(this.shelves)) {
            const foundBook = shelf.find(book => book.id === bookId);
            if (foundBook) {
                return foundBook;
            }
        }

        return null; // Книга не найдена
    }
}

const library = new Library();

module.exports = library;