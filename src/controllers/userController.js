const jwt = require("jsonwebtoken");

class User {
    constructor() {
        this.users = [
            { id: 1, username: 'admin', password: 'admin', role: 'librarian'},
            { id: 2, username: 'user', password: 'user', role: 'reader', userBooks: []}
        ];
    }

    createUser(username, password, role) {
        if (this.users.some(user => user.username === username)) {
            throw new Error('Username is already taken. Please choose a different username.');
        }

        const newUser = { id: this.users.length + 1, username, password, role, userBooks: [] };
        this.users.push(newUser);
        return newUser;
    }

    getUsers() {
        return this.users;
    }

    getUserById(userId) {
        return this.users.find(user => user.id === userId);
    }

    getUserByUsernameAndPassword(username, password) {
        return this.users.find(user => user.username === username && user.password === password);
    }

    borrowBook(userId, book) {
        const user = this.getUserById(userId);

        if (!user) {
            throw new Error('User not found.');
        }

        user.userBooks.push(book);
    }

    returnBook(userId, bookId) {
        const user = this.getUserById(userId);

        if (!user) {
            throw new Error('User not found.');
        }

        const index = user.userBooks.findIndex(book => book.id === bookId);

        if (index !== -1) {
            user.userBooks.splice(index, 1);
        } else {
            throw new Error('Book not found in user\'s collection.');
        }
    }
}


const userManager = new User();

const secretKey = "secret_key";


function authenticateUser(req, res, next) {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ error: 'Unauthorized. Token not provided.' });
    }

    try {
        const decoded = jwt.verify(token, secretKey);
        req.user = decoded;
        next();
    } catch (error) {
        res.status(401).json({ error: 'Unauthorized. Invalid token.' });
    }
}

function authenticateToken(req, res, next) {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ error: 'Unauthorized. Token not provided.' });
    }

    jwt.verify(token, secretKey, (err, user) => {
        if (err) {
            return res.status(401).json({ error: 'Unauthorized. Invalid token.' });
        }

        req.user = user;
        next();
    });
}

module.exports = {authenticateUser, authenticateToken, userManager, secretKey};