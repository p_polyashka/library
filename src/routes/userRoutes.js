const jwt = require("jsonwebtoken");
const express = require("express");
const app = express();
const {userManager, secretKey,  authenticateToken} = require("../controllers/userController")
const {library} = require("../controllers/libraryController");

app.get('/users', (req, res) => {
    const users = userManager.getUsers();
    res.json(users);
});


app.post('/login', (req, res) => {
    const { username, password } = req.body;

    const user = userManager.getUserByUsernameAndPassword(username, password);

    if (!user) {
        return res.status(401).json({ error: 'Invalid credentials.' });
    }

    const token = jwt.sign({ id: user.id, username: user.username, role: user.role }, secretKey, { expiresIn: '1h' });

    res.json({ token });
});

app.post('/register', (req, res) => {
    const { username, password } = req.body;
    const role = "reader";

    if (!username || !password) {
        return res.status(400).json({ error: "Please provide a username and password." });
    }

    try {
        const newUser = userManager.createUser(username, password, role);
        res.json(newUser);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});



app.post('/user/:userId/borrow/:bookId', authenticateToken, (req, res) => {
    const { userId, bookId } = req.params;
    const currentUser = req.user;

    try {
        const user = userManager.getUserById(parseInt(userId));
        if (!user) {
            return res.status(404).json({ error: 'User not found.' });
        }

        if (currentUser.id !== user.id) {
            return res.status(403).json({ error: 'Permission denied. You can only borrow books for yourself.' });
        }

        const book = library.getBookById(parseInt(bookId));
        if (!book) {
            return res.status(404).json({ error: 'Book not found.' });
        }

        userManager.borrowBook(user.id, book);
        res.json({ success: true });
    } catch (error) {
        res.status(500).json({ error: 'Internal server error.' });
    }
});


app.post('/user/:userId/return/:bookId', authenticateToken, (req, res) => {
    const { userId, bookId } = req.params;
    const currentUser = req.user;

    try {
        const user = userManager.getUserById(parseInt(userId));
        if (!user) {
            return res.status(404).json({ error: 'User not found.' });
        }

        if (currentUser.id !== user.id) {
            return res.status(403).json({ error: 'Permission denied. You can only return books for yourself.' });
        }

        const book = library.getBookById(parseInt(bookId));
        if (!book) {
            return res.status(404).json({ error: 'Book not found.' });
        }

        userManager.returnBook(user.id, book.id);
        res.json({ success: true });
    } catch (error) {
        res.status(500).json({ error: 'Internal server error.' });
    }
});


module.exports = app;