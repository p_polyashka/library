const express = require("express");
const app = express();
const {authenticateUser, authenticateToken, userManager} = require("../controllers/userController");
const library = require("../controllers/libraryController")

app.post('/books/move', authenticateUser, (req, res) => {
    const { user } = req;
    const { bookId, currentShelfId, targetShelfId } = req.body;

    if (user.role !== 'librarian') {
        return res.status(403).json({ error: 'Permission denied. You do not have the necessary privileges.' });
    }

    try {
        library.moveBookToShelf(user, bookId, currentShelfId, targetShelfId);
        res.json({ success: true });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});

app.post('/books/create', authenticateToken, (req, res) => {
        const { title, author, genre, isbn } = req.body;

    const currentUser = userManager.users.find(user => user.username === req.user.username);

    if (!currentUser || currentUser.role !== 'librarian') {
        return res.status(403).json({ error: 'Permission denied. You do not have the necessary privileges.' });
    }

    if (!title || !author || !genre || !isbn) {
        return res.status(400).json({ error: 'All fields are required.' });
    }
    const newBook = library.createBook(title, author, genre, isbn);
    res.status(201).json(newBook);
});

app.post('/shelves/create', authenticateToken, (req, res) => {
    const currentUser = req.user;

    if (!currentUser || currentUser.role !== 'librarian') {
        return res.status(403).json({ error: 'Permission denied. You do not have the necessary privileges.' });
    }

    const { genre } = req.body;

    if (!genre) {
        return res.status(400).json({ error: 'Genre is required.' });
    }

    try {
        const result = library.createShelf(genre);
        res.status(201).json(result);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
});



app.get('/books/search', (req, res) => {
    const { title, author, genre, id, isbn } = req.query;

    const searchCriteria = { title, author, genre, id, isbn };
    const searchResults = library.searchBooks(searchCriteria);

    if (searchResults.length === 0) {
        return res.status(404).json({ error: 'No books found.' });
    }

    res.json(searchResults);
});

app.get('/books', (req, res) => {
    res.json(library.getAllBooks());
});

app.get('/shelves', (req, res) => {
    res.json(library.getAllShelves());
});




module.exports = app;